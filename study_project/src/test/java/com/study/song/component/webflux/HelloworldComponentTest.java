package com.study.song.component.webflux;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

@ExtendWith(SpringExtension.class)
@WebFluxTest(HelloworldComponent.class)
public class HelloworldComponentTest {
    
    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void test_helloworld() {
        final String RESPONSE_BODY = webTestClient.get().uri("/helloworld").exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .returnResult().getResponseBody();

        assertEquals("Helloworld", RESPONSE_BODY);
    }

}