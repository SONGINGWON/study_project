package com.study.song.config.webflux;

import com.study.song.component.webflux.HelloworldComponent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

@Configuration
@EnableWebFlux
public class HelloworldConfig implements WebFluxConfigurer{
    
    @Bean
    public RouterFunction<ServerResponse> routes(HelloworldComponent helloworldComponent) {
        return RouterFunctions.route(GET("/helloworld"), helloworldComponent::helloworld);
    }

}