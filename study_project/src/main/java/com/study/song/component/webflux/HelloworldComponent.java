package com.study.song.component.webflux;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

@Component
public class HelloworldComponent {
    
    public Mono<ServerResponse> helloworld(ServerRequest request) {
        final String HELLO = "Helloworld";
        final Mono<String> HELLO_MONO = Mono.just(HELLO);

        return ServerResponse.ok().body(HELLO_MONO, String.class);
    }

}
