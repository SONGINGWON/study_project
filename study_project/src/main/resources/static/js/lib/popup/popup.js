(function (factory) {
    const scripts = Object.values(document.getElementsByTagName('script'));
    let isExistJqueryVer3_4_1 = false;
    let isExistBootstrapVer4_4_1 = false;
        
    scripts.forEach(function (script) {
        if (script.src === "/webjars/jquery/3.4.1/jquery.min.js"){
            isExistJqueryVer3_4_1 = true;
        } else if (script.src === "/webjars/bootstrap/4.4.1/js/bootstrap.min.js"){
            isExistBootstrapVer4_4_1 = true;
        }
    });  

    const head = document.getElementsByTagName("head").item(0);
    if (!isExistJqueryVer3_4_1){
        const jquery = document.createElement("script");
        jquery.type = "text/javascript";
        jquery.src = "/webjars/jquery/3.4.1/jquery.min.js";
        head.appendChild(jquery);
    }
    if (!isExistBootstrapVer4_4_1){
        const bootstrap = document.createElement("script");
        bootstrap.type = "text/javascript";
        bootstrap.src = "/webjars/bootstrap/4.4.1/js/bootstrap.min.js";
        head.appendChild(bootstrap);
    }

    factory(jQuery);
})(function ($) {
    
});